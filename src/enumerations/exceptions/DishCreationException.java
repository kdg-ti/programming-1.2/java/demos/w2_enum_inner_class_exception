package enumerations.exceptions;

public class DishCreationException extends Exception{
  public DishCreationException(String msg){
    super(msg);
  }

  public DishCreationException(String msg,Throwable cause){
    super(msg,cause);
  }
}
