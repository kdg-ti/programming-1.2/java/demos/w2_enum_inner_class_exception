package enumerations;

public class Dish {
  private String  name;
  private int persons;
  private Spice spice;

  public Dish(String name, int persons, Spice spice) {
    setName(name);
    this.persons = persons;
    this.spice = spice;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    if (name == null){
      throw new NullPointerException("Name should be present ");

    }
    else if( name.length() < 3) {
      //throw new IllegalArgumentException("Name should be present and longer than 3 characters");
      throw new IllegalArgumentException("Name should be longer than 3 characters");
    }
    this.name = name;
  }

  public int getPersons() {
    return persons;
  }

  public void setPersons(int persons) {
    this.persons = persons;
  }

  public Spice getSpice() {
    return spice;
  }

  public void setSpice(Spice spice) {
    this.spice = spice;
  }

  @Override
  public String toString() {
    return "Dish{" +
        "name='" + name + '\'' +
        ", persons=" + persons +
        ", spice='" + spice + '\'' +
        '}';
  }
}
