package enumerations;

public enum Spice {
  THYME(1),PEPPER(7),CURRY(3),SALT(1);
  private int spiciness;

   Spice(int spiciness) {
    this.spiciness = spiciness;
  }

  public int getSpiciness() {
    return spiciness;
  }

  @Override
  public String toString() {
    return "Spice{" +
        "ordinal=" + ordinal() +
        ", spiciness=" + spiciness +
        "} " + super.toString();
  }


}
