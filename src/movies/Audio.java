package movies;

public enum Audio {
  PCM, DOLBY, DOLBY_HD, VHS,  DTS_HD
}
