//import static enumerations.Spice.PEPPER;
//import static enumerations.Spice.THYME;
import static enumerations.Spice.*;

import enumerations.Dish;
import enumerations.Spice;
import enumerations.exceptions.DishCreationException;
import java.util.List;
import java.util.Random;

public class Runner {
  public static void main(String[] args) throws DishCreationException {

    List<Dish> dishes = null;
    try {
      dishes = List.of(
          new Dish(null, 4, PEPPER),
          new Dish("waterzooi", 4, THYME),
          new Dish("teriyaki burger", 4, PEPPER)

          );
    } catch (IllegalArgumentException| NullPointerException e) {
      System.err.println("Creation of your dishes failed, but try our other goodies!");
      dishes = List.of( new Dish("fries", 1, SALT));
      throw new DishCreationException("Creation failed",e);
  }
//    catch (NullPointerException e){
//      throw new DishCreationException("Creation failed",e);
//    }
    System.out.print("You can choose from these spices: ");
    for (Spice value : Spice.values()) {
      System.out.print(value + ", ");
    }
    System.out.println();
    for (Dish dish : dishes) {
      if (dish.getSpice()== PEPPER){
        System.out.println(dish);
      }
    }

    System.out.println("Non spicy dishes: ");
    for (Dish dish : dishes) {
      if (dish.getSpice().getSpiciness()< 5){
        System.out.println(dish);
      }
    }
    System.out.print("A random spice: ");
    Random random = new Random();
    Spice[] spices = Spice.values();
    System.out.println(spices[random.nextInt(spices.length)]);


  }
}
